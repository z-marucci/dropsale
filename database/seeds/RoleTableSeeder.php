<?php

use App\Role;

class RoleTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $role = new Role;
        $role->name = 'admin';
        $role->display_name = 'Administrator';
        $role->description = 'Admin with all roles';
        $role->save();
    }
}
