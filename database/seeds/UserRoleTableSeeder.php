<?php

use App\Role;
use App\User;

class UserRoleTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $user = User::find(1);
        $role = Role::find(1);
        $user->attachRole($role);
        $user->save();
    }
}
