<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDropsalePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dropsale_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dropsale_id')->unsigned();
            $table->foreign('dropsale_id')->references('id')->on('dropsales')->onDelete('cascade');
            $table->string('thumbnail_path');
            $table->string('name');
            $table->string('path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dropsale_photos');
    }
}
