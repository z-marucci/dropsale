<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyCountryStreetDescriptionColumnsToDropsalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dropsales', function (Blueprint $table) {
            //
            $table->string('title', 100)->change();
            $table->string('street', 500)->change();
            $table->text('description', 2000)->change();
            $table->string('city', 100)->change();
            $table->string('state', 100)->change();
            $table->string('country')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dropsales', function (Blueprint $table) {
            //
        });
    }
}
