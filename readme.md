# Dropsale
A simple easy to use image-based market place currently in its beta phase. Updates will be added regularly.

## Includes
- Bootstrap Sass
- Gulp image min
- Fontawesome
- Gulp sass
- Lightbox
- Dropzone
- Jquery
- Bower
- Gulp

### Todo List
- Add an admin dashboard complete with roles and permission
- Add Flash messaging for better feedback on forms
- Add in mail features such as forgot password and registration link sent via email
- Adding basic chart usage and helpful statistics

#### Credits and acknowledgement
This project is inspired greatly by the [Build Project Flyer with me](https://laracasts.com/series/build-project-flyer-with-me)
