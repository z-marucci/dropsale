<?php
	namespace App\Http;
	
	class Flash {

		public function create($type, $title, $text, $showConfirmationButton = false, $timer = false, $key = 'flash_message')
		{
			 return session()->flash($key, [
			 	'type' => $type,
			 	'title' => $title,
			 	'text' => $text,
			 	'timer' => $timer,
			 	'showConfirmationButton' => $showConfirmationButton
			 ]);
		}

		public function success($title, $text, $showConfirmationButton = false, $timer = false)
		{
			return $this->create('success', $title, $text, $showConfirmationButton, $timer);	 
		}

		public function error($title, $text, $showConfirmationButton = false, $timer = false)
		{
			return $this->create('error', $title, $text, $showConfirmationButton, $timer);	 
		}
	}