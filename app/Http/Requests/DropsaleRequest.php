<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DropsaleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title' => 'required|max:150|min:5',
            'country' => 'required',
            'price' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'description' => 'required|max:2500|min:10',
            'street' => 'required|max:250|min:2',
            'city' => 'required|regex:/^[\pL\s\-]+$/u|max:100|min:2',
            'zip' => 'max:15|min:2',
            'state' => 'regex:/^[\pL\s\-]+$/u|max:100|min:2'
        ];
    }
}
