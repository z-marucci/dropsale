<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dropsale;
use App\Photo;
use App\Http\Requests;
use App\AddPhotoToDropsale;
use App\Http\Controllers\Controller;
use App\Http\Requests\PhotosRequest;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function userCreatedDropsale(Request $request)
    {
        return Dropsale::where([
            'user_id' => $this->user->id,
            'id' => $request->id,
        ])->exists();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, PhotosRequest $request)
    {
        $dropsale = Dropsale::getByDropsale($id);
        $photo = $request->file('photo');
        if (!$this->userCreatedDropsale($request)) {
            return 'not authorized';
        }

        (new AddPhotoToDropsale($dropsale, $photo))->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PhotosRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $photo = Photo::find($id);
        $photo->delete();
        return redirect()->back();
    }
}
