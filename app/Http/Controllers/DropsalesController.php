<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers;
Use App\Dropsale;
use App\Http\Requests;
use App\Http\Requests\DropsaleRequest;
use App\Http\Controllers\Controller;

class DropsalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        parent::__construct();
    }

    public function index()
    {
        //
        $dropsales = Dropsale::orderBy('created_at', 'desc')->get();
        return view('dropsales.index', compact('dropsales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('dropsales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DropsaleRequest $request)
    {
        //
        $dropsale = $this->user->attachDropsale(new Dropsale($request->all()));
        $dropsale->save();
        // flash()->success('Success!', 'A new dropsale has been created', 'OK', 2000);
        return redirect(dropsale_id_path($dropsale));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Gets dropsale by id
        $dropsale = Dropsale::getByDropsale($id);
        return view('dropsales.show', compact('dropsale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $dropsale = Dropsale::getByDropsale($id);
        if($this->user && $this->user->owns($dropsale)) {
            return view('dropsales.edit', compact('dropsale'));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DropsaleRequest $request, $id)
    {
        //
        $dropsale = Dropsale::getByDropsale($id);
        $dropsale->update(request()->all());
        // flash()->success('Success!', 'Your dropsale was update!', false);
        return redirect("/dropsales/".$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
