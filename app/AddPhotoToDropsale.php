<?php 

namespace App;

use App\Dropsale;
use App\Thumbnail;
use App\Photo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

Class AddPhotoToDropsale {
    protected $dropsale;
    protected $file;

    public function __construct(Dropsale $dropsale, UploadedFile $file, Thumbnail $thumbnail = null)
    {
         $this->dropsale = $dropsale;
         $this->file = $file;
         $this->thumbnail = $thumbnail ?: new Thumbnail;
    }

    public function save()
    {
        // Attach the photo to the dropsale

        $photo = $this->dropsale->addPhoto($this->makePhoto());
        // Move the photo to its directory

        $this->file->move($photo->baseDir(), $photo->name);
        // Create a thumbnail
        $this->thumbnail->make($photo->path, $photo->thumbnail_path);
    }

    public function makePhoto()
    {   
        return new Photo(['name' => $this->makeFileName()]);
    }

    protected function makeFileName()
    {
        $hash = sha1(
            time() . $this->file->getClientOriginalName()
        );
        $extension = $this->file->getClientOriginalExtension();
        return $this->name = "{$hash}.{$extension}";
    }
}

 ?>