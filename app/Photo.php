<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
    protected $fillable = ['name', 'path', 'thumbnail_path'];
    protected $table = 'dropsale_photos';
    public function baseDir()
    {
        return 'dropsale/photos';
    }

    public function setNameAttribute($name)
    {
         $this->attributes['name'] = $name;

         $this->path = $this->baseDir().'/'.$name;
         $this->thumbnail_path = $this->baseDir().'/tn-'.$name;
    }

    public function delete()
    {
         \File::delete([
            $this->path,
            $this->thumbnail_path
         ]);

         parent::delete();
    }
}
