<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Dropsale extends Model
{
    //
    protected $fillable = [
    	'street',
    	'city',
    	'state',
    	'price',
    	'zip',
    	'description',
    	'country',
    	'price',
    	'title'
    ];


    public function addPhoto(Photo $photo) {
        return $this->photos()->save($photo);
    }

    public function photos() {
        return $this->hasMany('App\Photo');
    }

    public static function getByDropsale($id)
    {
        $id = intval($id);
        try {
            $dropsale = Dropsale::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
        
        return  $dropsale;
    }

    public function ownedBy()
    {
        return $this->belongsTo('App\User', 'id');
    }
}
