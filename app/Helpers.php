<?php 
    
    function dropsale_id_path(App\Dropsale $dropsale)
    {   
        $path = 'dropsales/'.$dropsale->id; 
        return $path;
    }

    function get_first_photo(App\Dropsale $dropsale)
    {   $firstPhoto = $dropsale->photos->first();
        if (is_null($firstPhoto)) {
            return false;
        }

        return $firstPhoto->thumbnail_path;
    }

    function has_image(App\Dropsale $dropsale)
    {
        $firstPhoto = $dropsale->photos->first();
        if (is_null($firstPhoto)) {
            return false;
        }
        return true;
    }

    function get_country_name($code)
    {
        return App\Utilities\Country::getCountryName($code);
    }

    function get_hero_image(App\Dropsale $dropsale)
    {   $firstPhoto = $dropsale->photos->first();
        if (is_null($firstPhoto)) {
            return false;
        }

        return $firstPhoto->path;
    }

    function has_owner(App\Dropsale $dropsale)
    {
        return $dropsale->ownedBy->id;
    }

    function flash ($title = null, $message = null)
    {
        $flash = app('App\Http\Flash');

        if (func_num_args() == 0) {
            return $flash;
        }

        return $flash->info($title, $message);
    }

    function trim_title($title) {
       return mb_strimwidth($title, 0, 50, "...");
    }
?>