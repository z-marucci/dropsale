@extends('hero-layout')
	
@section('content')
	@include('dropsales.partials.hero', 
		[
			'has_image' => false,
			'image' => 'landing.jpeg',
			'hero_heading' => 'Administrator dashboard'
		])
	<div class="index--main">
		<div class="container">

			<div class="row">
				<div class="col-xs-12">
					<h2 class="text-center">Statistics</h2>
					<hr>
				</div>
			</div>

			<div class="row">
				<div class="col-md-5">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3>Current Users</h3>
						</div>
						<div class="panel-body">
							 <i class="pull-left fa fa-users fa-3x"></i><h3 class="pull-right"> {{$stats['users']}}</h3>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3>Current Dropsales</h3>
						</div>
						<div class="panel-body">
							 <i class="pull-left fa fa-book fa-3x"></i><h3 class="pull-right"> {{$stats['dropsales']}}</h3>
						</div>
					</div>
				</div>

			</div>

			<hr>

			<div class="row">
				<div class="col-xs-12">
					<h2>Actions</h2>
				</div>

				<div class="col-md-4">
					<a href="" class="btn-success btn-lg btn-block">Users</a>
				</div>

				<div class="col-md-4">
					<a href="" class="btn-success btn-lg btn-block">Users</a>
				</div>

				<div class="col-md-4">
					<a href="" class="btn-success btn-lg btn-block">Users</a>
				</div>

			</div>

			<hr>
		</div>
	</div>
@endsection