@extends('hero-layout')
	
	@section('content')

		@if((get_first_photo($dropsale)))
			@include('dropsales.partials.hero', 
				[
					'hero_heading' => trim_title($dropsale->title),
					'has_image' => has_image($dropsale),
					'path' => '/'.get_hero_image($dropsale)
				])
		@else 
			@include('dropsales.partials.hero', 
				[
					'hero_heading' => trim_title($dropsale->title),
					'has_image' => false,
					'image' => 'contact.jpeg',
				])
		@endif

	<hr>
	<div class="container">
		
	<div class="row">
		<div class="col-md-6">
			<div class="show--dropsale-grid">

				@if((get_first_photo($dropsale)))
					@foreach($dropsale->photos->chunk(4) as $set)
						<div class="row">
							@foreach($set as $photo)
								
								<div class="col-xs-3 show--dropsale-item">

									<form action = "/photos/{{$photo->id}}"" method = "POST">
										{{csrf_field()}}
										{{method_field('delete')}}

										<button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
									</form>
									<a data-lightbox="image" href="/{{$photo->path}}">
										<img class="img-responsive" src="/{{$photo->thumbnail_path}}" alt="">
									</a>
								</div>
							@endforeach
						</div>
						<hr>
					@endforeach

				@else
					<img class="img-responsive" src="/site-images/missing_image.png" alt="">
				@endif

				
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="panel panel-success text-center">
				<div class="panel-heading">
					<h2>At a glance:
						@if($user && $user->owns($dropsale))
							<a href="/dropsales/{{$dropsale->id}}/edit"><i class="fa pull-right fa-pencil"></i></a>
						@endif
					</h2>
				</div> 
				<div class="panel-body">
					
					<h2><i class="fa fa-usd"></i> {{number_format($dropsale->price, 2)}}</h2>

					<h2><i class="fa fa-location-arrow"></i>  {{$dropsale->street}}</h2>
					
					<h2> <i class="fa fa-globe"></i> {{get_country_name($dropsale->country)}}</h2>

					<h2> <i class="fa fa-building"></i> {{$dropsale->city}}</h2>


					<h2>{{$dropsale->state}}, {{$dropsale->zip}}</h2>
				</div>
			</div>
		</div>

		<div class="col-xs-12">

			
			@if($user && $user->owns($dropsale))
				<form id="addPhotosForm" class="dropzone" action="/{{$dropsale->id}}/photos/">
					{{csrf_field()}}
				</form>
			@endif
		</div>

		<hr>

		<div class="col-xs-12">

			<div class="panel panel-info">

				<div class="panel-heading">
					<p class="lead text-center">About this dropsale: {{$dropsale->title}}</p>
				</div>
				<div class="panel-body">
					<p>{!! nl2br($dropsale->description) !!}</p>
				</div>
			</div>
		</div>
		
	</div>
</div>
@endsection

@section('scripts.footer')
<script src=/js/libs.js></script>
<script>
	Dropzone.options.addPhotosForm = {
		paramName: 'photo',
		maxFilesize: 2,
		acceptedFiles: '.jpg, .jpeg, .png, .bmp, .gif'
	}

	lightbox.option({
	      'resizeDuration': 200,
	      'wrapAround': true
	    })
</script>