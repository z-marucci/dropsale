@inject('countries', 'App\Utilities\Country')
@extends('hero-layout')
	
	@section('content')
		@include('dropsales.partials.hero', 
			[
				'has_image' => false,
				'hero_heading' => 'Create a new dropsale',
				'image' => 'create.jpeg'
			])

		<div class="create--main">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						@if(count($errors) > 0)
						
							{{flash()->error('Error!', 'Looks like you have some errors, please go over them', 'OK')}}

							<div class="alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif


						<form action="/dropsales" method="POST" enctype="multipart/form-data">
							
							{{ csrf_field() }}

							<div class="row">

								<div class="col-md-6">
									<div class="form-group">
										<label for="title">Title *</label>
										<input type="text" name="title" value="{{old('title')}}"class="form-control" >
									</div>

									<div class="form-group">
										<label for="description">Description *</label>
										<textarea rows="5" class="form-control"  name="description">
												
										</textarea>
									</div>

									<div class="form-group">
										<label for="city">City *</label>
										<input type="text" cols="30" name="city" value="{{old('city')}}" class="form-control" >
									</div>

									<div class="form-group">
										<label for="price">Price *</label>
										<input type="text" name="price" value="{{old('price')}}"class="form-control" >
									</div>

									<div class="form-group">
										<label for="country">Country *</label>
										<select name="country"  id="country" value="{{old('country')}}" class="form-control">
											@foreach($countries::all() as $country => $code)
												<option value="{{$code}}">{{$country}}</option>
											@endforeach
										</select>
									</div>

									<div class="form-group">
										<label for="street">Address / Street *</label>
										<input type="text" name="street" value="{{old('street')}}"class="form-control" >
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="state">State / Province</label>
										<input type="text" name="state" value="{{old('state')}}"class="form-control">
									</div>


									<div class="form-group">
										<label for="zip">Zipcode</label>
										<input type="text" name="zip" value="{{old('zip')}}"class="form-control">
									</div>

									<div class="form-group">
										<button type="submit" class="btn btn-success btn-lg">Create dropsale!</button>
									</div>
								</div>
							</div>
						</form>
						
					</div>
					</div>
				
			</div>
		</div>
	@stop