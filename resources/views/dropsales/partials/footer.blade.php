<div class="container">
	<div class="footer--container col-xs-12">
		<ul class="footer-nav">
			
		</ul>						
		<h2>Need to get in touch?</h2>
		<h3>Here's How</h3>
	</div>
	<div class="footer-icons">
		<div class="col-xs-6">
			<a target="_blank" href="https://gitlab.com/z-marucci">
				<i class="fa fa-gitlab fa-5x"></i>
			</a>
		</div>
		<div class="col-xs-6">
			<a target="_blank" href="https://pa.linkedin.com/in/angelo-marucci">
				<i class="fa fa-linkedin fa-5x"></i>
			</a>
		</div>
	</div>
</div>