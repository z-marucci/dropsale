@inject('countries', 'App\Utilities\Country')
@extends('hero-layout')
	
	@section('content')
		@if((get_first_photo($dropsale)))
			@include('dropsales.partials.hero', 
				[
					'hero_heading' => trim_title($dropsale->title),
					'has_image' => has_image($dropsale),
					'path' => '/'.get_hero_image($dropsale)
				])
		@else 
			@include('dropsales.partials.hero', 
				[
					'hero_heading' => trim_title($dropsale->title),
					'has_image' => false,
					'image' => 'contact.jpeg',
				])
		@endif

		<div class="create--main">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						@if(count($errors) > 0)
						
							{{flash()->error('Error!', 'Looks like you have some errors, please go over them', 'OK')}}

							<div class="alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif


						<form action="/dropsales/{{$dropsale->id}}" method="POST" enctype="multipart/form-data">
							
							{{ csrf_field() }}
							{{ method_field('PATCH') }}

							<div class="row">

								<div class="col-md-6">
									<div class="form-group">
										<label for="title">Title *</label>
										<input type="text" name="title" value="{{$dropsale->title}}"class="form-control" >
									</div>

									<div class="form-group">
										<label for="description">Description *</label>
										<textarea class="form-control"  name="description" id="" rows="5">
												{{$dropsale->description}}	
										</textarea>
									</div>

									<div class="form-group">
										<label for="city">City *</label>
										<input type="text" name="city" value="{{$dropsale->city}}"class="form-control" >
									</div>

									<div class="form-group">
										<label for="price">Price *</label>
										<input type="text" name="price" value="{{$dropsale->price}}"class="form-control" >
									</div>

									<div class="form-group">
										<label for="country">Country *</label>
										<select name="country"  id="country" value="{{old('country')}}" class="form-control">
											@foreach($countries::all() as $country => $code)
												<option value="{{$code}}">{{$country}}</option>
											@endforeach
										</select>
									</div>

									<div class="form-group">
										<label for="street">Address / Street *</label>
										<input type="text" name="street" value="{{$dropsale->street}}"class="form-control" >
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="state">State / Province</label>
										<input type="text" name="state" value="{{$dropsale->state}}"class="form-control">
									</div>


									<div class="form-group">
										<label for="zip">Zipcode</label>
										<input type="text" name="zip" value="{{$dropsale->zip}}"class="form-control">
									</div>

									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-lg">Update your dropsale!</button>
									</div>
								</div>
							</div>
						</form>
						
					</div>
					</div>
				
			</div>
		</div>
	@stop