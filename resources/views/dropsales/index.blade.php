@extends('hero-layout')

@section('content')

	@include('dropsales.partials.hero', 
		[
			'hero_heading' => 'A very special Dropsale is waiting for you',
			'image' => 'index.jpeg',
			'has_image' => false
		])
	<div class="container">
		<div class="row">
		
		<div class="col-xs-12">
			@if (Auth::user())
				<a href="/dropsales/create" class="btn btn-success btn-lg">Create new dropsale!</a>
			@else 
				<h2><a href="auth/login">Sign in to create your very first dropsales!</a></h2>
			@endif

			<hr>
			@if (isset($dropsales))
				@foreach($dropsales as $dropsale)
					<div class="panel panel-info">
						<div class="panel-heading">
							<a href="/{{dropsale_id_path($dropsale)}}"><h3>{{$dropsale->title}} - ${{number_format($dropsale->price, 2)}}</h3>
							</a>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									@if((get_first_photo($dropsale)))
										<img src="{{get_first_photo($dropsale)}}" alt="">

									@else
										<img src="img/no-photo.png" alt="">
									@endif
								</div>

								<div class="col-md-9">
									<p class="lead">Description:</p>
									<p>{{$dropsale->description}}</p>
									<p class="lead">Address:</p>
									<p>{{$dropsale->street}}</p>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				@else 
					<h2>There are now Dropsales at the moment!</h2>
			@endif
			
		</div>

		</div>
		
	</div>
@endsection