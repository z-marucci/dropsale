<!-- resources/views/auth/login.blade.php -->

@include('dropsales.partials.errors')
    

    <html>
    <head>
        <meta charset="UTF-8">
        <title>Dropsale</title>
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/libs.css">
        <meta charset="UTF-8" name="viewport" content="width=device-width">
    </head>
    <body class="login--body">
        <div class="container-fluid">
            <div class="container">
                <div class="jumbotron login--jumbotron">
                    <h1>Login to start dropping!</h1>

                    <form method="POST" action="/auth/login">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            Email
                            <input required class="form-control" type="email" name="email" value="{{ old('email') }}">
                        </div>

                        <div class="form-group">
                            Password
                            <input required class="form-control" type="password" name="password" id="password">
                        </div>
                        
                        <hr>

                        <div class="form-group">
                            <input class="" type="checkbox" name="remember"> Remember Me
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success btn-lg btn-block"type="submit">Login</button>
                        </div>

                        <hr>

                        <a href="/auth/register" class="btn btn-info btn-lg btn-warning btn-block">New to Dropsale?</a>
                    </form>
                </div>

                <div class="layout--copyright">
                    @include('dropsales.partials.copyright')
                </div>

            </div>
        </div>
        
    @section('footer.scripts')
        <script src=/js/libs.js></script>
    @endsection
        
    </body>
</html>
