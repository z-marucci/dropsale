
@include('dropsales.partials.errors')

<html>
    <head>
        <meta charset="UTF-8">
        <title>Dropsale</title>
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/libs.css">
        <meta charset="UTF-8" name="viewport" content="width=device-width">
    </head>
    <!-- resources/views/auth/register.blade.php -->
    <body class="sign-up--body">
            <form method="POST" action="/auth/register">
                {!! csrf_field() !!}
                <div class="container-fluid">
                    <div class="container">
                        <div class="jumbotron login--jumbotron">
                            <h1>Let's get you started</h1>

                            <div class="form-group">
                                Name
                                <input required class="form-control" type="text" name="name" value="{{ old('name') }}">
                            </div>

                            <div class="form-group">
                                Email
                                <input required class="form-control" type="email" name="email" value="{{ old('email') }}">
                            </div>

                            <div class="form-group">
                                Password
                                <input required class="form-control" type="password" name="password">
                            </div>

                            <div class="form-group">
                                Confirm Password
                                <input required class="form-control" type="password" name="password_confirmation">
                            </div>

                            <div class="form-group">
                                <button class="btn btn-success btn-lg btn-block"type="submit">Register</button>
                            </div>

                            <hr>

                            <a href="/auth/login" class="btn btn-warning btn-lg btn-block">I already have a Dropsale account</a>
                        </div>

                        <div class="layout--copyright">
                            @include('dropsales.partials.copyright')
                        </div>

                    </div>
                </div>
            </form>
    </body>
</html>