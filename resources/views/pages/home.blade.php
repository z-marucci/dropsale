@include('pages.partials.head')
	<body class="home--body">
		<div class="container-fluid">
			<div class="container">
				<div class="jumbotron home--jumbotron">
					<h1>Welcome to Dropsale!</h1>
					<p class="lead">
						Dropsale is a simple to use image-based marketplace, currently in its beta phase. Its easy to get started, create your account, create a new drop, and you're ready to go.
					</p>
					<p>Don't miss out - Come on in and start dropping!</p>

					@if ($signedIn)
						<a href="/dropsales/create" class="btn btn-success btn-block btn-lg">Start Dropsaling!</a>
						<hr>
						<a href="dropsales" class="btn btn-info btn-lg btn-block">See our list of dropsales!</a>
					@else
						<a href="/auth/register" class="btn btn-success btn-block btn-lg btn-block">Sign up now!</a>
						<hr>
						<a href="auth/login" class="btn btn-info btn-lg btn-block">I'm already a dropper</a>
					@endif
				</div>

				<div class="layout--copyright">
				    @include('dropsales.partials.copyright')
				</div>

			</div>
		</div>
		@section('footer.scripts')
			<script src=/js/libs.js></script>
		@endsection
		
	</body>
</html>
