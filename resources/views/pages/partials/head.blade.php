<html>
<!-- Partials -->
<head>
    <meta charset="UTF-8">
    <meta charset="UTF-8" name="viewport" content="width=device-width">
    <meta name="description" content="Dropsale, best place for local bargains, sales and offers!">
    <meta name="author" content="Zealotscout">
    <title>Dropsale</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/libs.css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106907230-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-106907230-2');
    </script>
</head>
