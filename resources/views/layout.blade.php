@include('pages.partials.head')
<body class="body--main">
    @include ('flash')
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Dropsale</a>
        </div>
        
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
                @if(Auth::user())
                    <li class="">
                        <a>Hello, {{Auth::user()->name}}</a>
                    </li>

                    <li class="">
                        <a href="/auth/logout">Logout</a>
                    </li>
                @else
                    <li class="">
                        <a href="/auth/login">Login</a>
                    </li>
                @endif

                
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
        @yield('content')

        @yield('scripts.footer')
    </div>

    <div class="layout--footer">
        @include('dropsales.partials.footer')
    </div>
    
    <div class="layout--copyright">
        @include('dropsales.partials.copyright')
    </div>
    
</body>
</html>