@if (session()->has('flash_message'))

<script src=/js/libs.js></script>
    <script>
        swal({
            title: "{{session('flash_message.title')}}",
            text: "{{ session('flash_message.text') }}",
            type: "{{session('flash_message.type')}}",
            timer: "{{session('flash_message.timer')}}",
            showConfirmationButton: "{{session('flash_message.showConfirmationButton')}}"
        });
    </script>
@endif