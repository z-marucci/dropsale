var elixir = require('laravel-elixir');
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task('imgcrush', () =>
    gulp.src('resources/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/site-images'))
);

elixir(function(mix) {
    mix.sass('app.scss')
        .copy('bower_components/jquery/dist/jquery.min.js', 'resources/assets/js/libs/jquery.js')
        .copy('node_modules/sweetalert2/dist/sweetalert2.css', 'resources/assets/css/libs/sweetalert.css')
        .copy('node_modules/sweetalert2/dist/sweetalert2.js', 'resources/assets/js/libs/sweetalert.js')
        .copy('node_modules/sweetalert2/dist/sweetalert2.min.js', 'resources/assets/js/libs/sweetalert.min.js')
        .copy('bower_components/dropzone/dist/dropzone.js', 'resources/assets/js/libs/dropzone.js')
        .copy('bower_components/dropzone/dist/dropzone.css', 'resources/assets/css/libs/dropzone.css')
        .copy('node_modules/lity/dist/lity.js', 'resources/assets/js/libs/lity.js')
        .copy('node_modules/lity/dist/lity.css', 'resources/assets/css/libs/lity.css')
        .copy('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'resources/assets/js/libs/bootstrap.js')
        .copy('node_modules/lightbox2/dist/css/lightbox.css', 'resources/assets/css/libs/lightbox.css')
        .copy('node_modules/lightbox2/dist/js/lightbox.min.js', 'resources/assets/js/libs/lightbox.js')
        .scripts([
            'libs/jquery.js','libs/bootstrap.js', 'libs/sweetalert.js', 'libs/dropzone.js', 'libs/lity.js', 'libs/lightbox.js',
        ], './public/js/libs.js')
        .styles([
            'libs/sweetalert.css',
            'libs/dropzone.css',
            'libs/lity.css',
            'libs/lightbox.css'
        ], './public/css/libs.css')
    mix.task('imgcrush');
});
